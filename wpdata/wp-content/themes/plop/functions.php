<?php

/* Child theme declaration */

function plop_wp_enqueue_scripts() {

    $parenthandle = 'twentytwenty-style';
    $theme        = wp_get_theme();

    // Load parent CSS
    wp_enqueue_style(
        $parenthandle,
        get_template_directory_uri() . '/style.css', // https://plop.org/wp-content/themes/twentytwenty/style.css
        array(),
        $theme->parent()->get( 'Version' )
    );

    // Load child CSS (this theme)
    wp_enqueue_style(
        'plop-style',
        get_stylesheet_uri(), // https://plop.org/wp-content/themes/plop/style.css
        array( $parenthandle ),
        $theme->get( 'Version' )
    );
    
}

add_action( 'wp_enqueue_scripts', 'plop_wp_enqueue_scripts' );

function plop_register_post_type_project(){
    register_post_type(
        "project",
        array(
            "labels" => array(
                'name' => 'Projet',
                'singular_name' => 'Projets'
            ),
            'description' => true,
            'public' => true,
            'publicity_queryable' => true,
            'exclude_from_each' => true,
            'show_ui' => true,
            'show_in_rest' => true,
            'hierarchical' => true,
            'supports' => array('title', 'editor', 'thumbnail','page-attributes'),
            'has_archive' => 'projets',
            'rewrite' => array('slug' => 'projet'),

        )
        );
}
add_action('init', 'plop_register_post_type_project', 10);